#!/usr/bin/env bash
# /usr/sbin/httpd -D FOREGROUND -f /etc/apache2/httpd.conf

WebConf=/etc/apache2/conf.d/spotweb.conf
SSLWebConf=/etc/apache2/conf.d/spotweb_ssl.conf
WebDir=/var/www/spotweb

echo
echo "Installing Spotweb webfiles from github:"
git init ${WebDir}
cd ${WebDir}
git remote add origin https://github.com/spotweb/spotweb.git

if [[ ! -z ${VERSION} ]]
then
  echo "Downloading Spotweb ${VERSION}:"
  git pull origin ${VERSION}
else
  echo "Downloading Spotweb master"
  git pull origin master
fi

echo

echo "date.timezone = ${TZ}" >> ""$PHP_INI_DIR/php.ini
echo
echo "Installing Spotweb Retrieve ${AUTO_RETRIEVE} crond script."
case ${AUTO_RETRIEVE} in
  15min)
    crontab -l -u www-data | { cat; echo "*/15 * * * * /opt/spotweb_retrieve.sh"; } | crontab -u www-data -
  ;;

  hourly)
    crontab -l -u www-data | { cat; echo "@hourly /opt/spotweb_retrieve.sh"; } | crontab -u www-data -
  ;;

  daily)
    crontab -l -u www-data | { cat; echo "0 0 * * * /opt/spotweb_retrieve.sh"; } | crontab -u www-data -
  ;;

  *)
    echo
    echo "Option AUTO_RETRIEVE=${AUTO_RETRIEVE} invalid, use 15min, hourly or daily"
  ;;
esac


echo "Deployment done!"
exec "$@"
