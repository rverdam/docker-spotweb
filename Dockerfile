FROM php:8.0-apache

ARG VCS_REF
LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url="https://gitlab.com/rverdam/docker-spotweb/"

COPY entry.sh /entry.sh
COPY dbsettings.inc.sqlite.php /root/dbsettings.inc.sqlite.php
COPY spotweb_retrieve.sh /opt/spotweb_retrieve.sh

RUN apt update && \
    apt-get install -y bash cron curl openssl git && \
    chmod +x /entry.sh && \
    chmod +x /opt/spotweb_retrieve.sh

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

ENV APACHE_RUN_DIR /run/apache2
ENV APACHE_DOCUMENT_ROOT /var/www/spotweb
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV AUTO_RETRIEVE daily

RUN mkdir -p /var/www/spotweb && chown -R www-data:www-data /var/www/spotweb

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

EXPOSE 80 443
ENTRYPOINT ["/entry.sh"]
VOLUME ["/var/www/spotweb"]
CMD ["/usr/sbin/apache2", "-D", "FOREGROUND"]
